Trabalho  - Criar um Webservice - Para cadeira de Arquitetura de Sistemas da UNIFOR
Autor: Gilberto Egypto

Trabalho foi feito utilizando Node.js para criar o webservice(Backend) aonde ele recebe uma requisição post no endereço
 localhost:5000/convert com o corpo

{
	"valor":500.00,
	"moeda":"dolar"
}


O valor a ser recebido em float 
A moeda deve informar ou "dolar" ou "euro" em string.

Caso seja feito uma requisição get no mesmo endereço a resposta recebida é a mesma explicação acima de como utilizar o Webservice.

Na api foi utilizado uma lógica que recebe o valor e a moeda e a converte utilizando a cotação do dia que foi obtida utilizando uma API
chamada "economia.awesomeapi", dessa forma ele não só serve como um Webservice como também se utiliza de outro webservice.

Além disso foi criado um Frontend em React.js que se conecta a esta API, fazendo todas as requisições necessárias através de um Fetch(), e
criando uma interface gráfica para que o usuário possa utilizar a aplicação.
