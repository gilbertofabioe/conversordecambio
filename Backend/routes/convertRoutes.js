const router = require('express').Router();


router.get('/', (req,res)=>{
    res.json({message: 'O pedido deve ser feito através de requisição Post,'+
    'O formato do body deve seguir a seguinte estrutura:'+
    '{'+
        '"valor": 17.00,'+
        '"moeda": "dolar"'+
    '}'+
    'O valor deve ser dado em float e a moeda pode ser "dolar" ou "euro".'
    });
});


router.post('/', async (req,res)=>{

    const {valor, moeda} = req.body;
    let euro;
    await fetch('https://economia.awesomeapi.com.br/json/last/USD-BRL,EUR-BRL').then(res=>res.text()).then(text=>{euro = JSON.parse(text).EURBRL.high});

    let dolar;
    await fetch('https://economia.awesomeapi.com.br/json/last/USD-BRL,EUR-BRL').then(res=>res.text()).then(text=>{dolar = JSON.parse(text).USDBRL.high});

    if(!valor || !moeda){
        res.status(422).json({error: 'Preencher todos os campos'});
        return;
    }else{

        if(moeda.toLowerCase() == "dolar"){
            const valorReal = valor*dolar;
            console.log(`Na cotação de hoje fica ${valorReal.toFixed(2)} reais`);
            res.status(200).json({msg: `Na cotação de hoje fica ${valorReal.toFixed(2)} Reais`});
            return;
        }else if(moeda.toLowerCase() == "euro"){
            const valorReal = valor*euro;
            console.log(`Na cotação de hoje fica ${valorReal.toFixed(2)} reais`);
            res.status(200).json({msg: `Na cotação de hoje fica ${valorReal.toFixed(2)} Reais`});
            return;
        };

    };

});


module.exports = router;