import './App.css';
import {useState, useEffect} from 'react'


const API = "http://localhost:5000/convert";

function App() {

  const [valor, setValor] = useState();
  const [moeda, setMoeda] = useState();
  const [convertido, setConvertido] = useState();

  const handleSubmit = async (e) =>{
    e.preventDefault();
    let valorReal;

    if(moeda == "null" || !moeda || valor == null){
      alert("Por favor escolher uma moeda!")
    }else{
        const envelope = {
          "valor": valor,
          "moeda": moeda
        }

        await fetch(API,{
          method: "POST",
          body: JSON.stringify(envelope),
          headers:{
            "Content-Type": "application/json",
          },
        }).then(res=>res.text()).then(text=>{valorReal = text}).then(() =>{
          setConvertido(JSON.parse(valorReal).msg)})
        
    }

  }

  return (
    <div className="App">
      <header className="App-header">
        <h1>Conversor de moeda</h1>
          <div className="converContainer">
            <h2>Converter aqui:</h2>
              <div className='valor'>
                <form className='conversor' onSubmit={handleSubmit}>
                  <div className='Input'>
                      <input 
                        type="number" 
                        placeholder='Informa o valor' 
                        className='valor'
                        min="0"
                        step="0.01"
                        value={ valor || "" }
                        onChange={(e)=> setValor(e.target.value)}
                        ></input>
                      <select className='moeda'
                        onChange={(e)=> setMoeda(e.target.value)}>
                        <option value="null">Escolha a moeda</option>
                        <option value="dolar">Dólar</option>
                        <option value="euro">Euro</option>
                      </select>
                  </div>
                  <div className='form-buttom-container'>
                    <input className='form-buttom' type="submit" value="Adicionar Gasto" />
                  </div>
                </form>
                <div className='resposta'>
                  <h2>{convertido}</h2>
                </div>
              </div>
          </div>
      </header>
    </div>
  );
}

export default App;

